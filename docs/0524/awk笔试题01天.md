1.用awk 打印整个test.txt (以下操作都是用awk工具实现，针对test.txt)
awk 1 test.txt

2.查找所有包含 ‘bash’ 的行
awk '/bash/' test.txt

3 .用 ‘:’ 作为分隔符，查找第三段等于0的行
awk -F':' '$3==0' test.txt

4.用 ‘:’ 作为分隔符，查找第一段为 ‘root’ 的行，并把该段的 ‘root’ 换成 ‘toor’ (可以连同sed一起使用)
awk -F':' '$1=="root"' test.txt | sed -n 's/root/toor/gp'
awk -F':' '$1=="root"{gsub(/root/, "toor", $0);print}' test.txt

5.用 ‘:’ 作为分隔符，打印最后一段

redis 文本
work   16067     /data/svr/redis/bin/redis-server*:6403
work   16067     /data/svr/redis/bin/redis-server*:6403
work   16067     /data/svr/redis/bin/redis-server*:6403
work   16067     /data/svr/redis/bin/redis-server*:6403

awk -F':' '{print $NF}' test.txt

6 
如何打印第二列pid 和最后一列端口打印出来 ，请给出命令

输出结果如下：
16067   6403
16067   6403
16067   6403
16067   6403

awk -F' +|:' '{print $2,$4}' test.txt

7  linux系统中如何获取 pid 9257的进程号监听的端口是什么  给出命令
ss -ntp | awk -F ' +|:' '/9257/{print $5}'

8  .查出实时 哪个IP地址连接最多 哪个ip 访问大于 1000次  截取出来
ss -ntu | awk '{print $5}' | awk -F':' '{print $1}'| sort |uniq -c |sort -nr | head -1
ss -ntu | awk '{print $5}' | awk -F':' '{print $1}'| sort |uniq -c |sort -nr | awk '$1>1000'

