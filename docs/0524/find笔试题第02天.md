1.找出/tmp目录下，属主不是root，且文件名不以f开头的文件
find /tmp -type f ! -user root ! -name 'f*'

2.查找/etc/目录下，所有.conf后缀的文件
find /etc -type f '*.conf'


3.查找/var目录下属主为root，且属组为mail的所有文件
find /var -type f -user root -group mail


4.查找/var目录下7天以前，同时属主不为root，也不是postfix的文件
find /var -type f ! -user root ! -user postfix -mtime 7


5.查找/etc目录下大于1M且类型为普通文件的所有文件
find /etc -type f -size +1M


6.查找/etc目录下所有用户都没有写权限的文件
find /etc -type f ! -perm /222


**7.查找/目录下最后创建时间是3天前，后缀是*.log的文件**
find / -type f -mtime +3 -name '*.log'


8.查找/目录下文件名包含txt的文件**
find / -type f -name '*txt*'


9.查找/目录下属主是oldboy并且属组是oldboy的文件
find / -type f -user oldboy -group oldboy

10.查找/目录下属主是oldboy但是属组不是oldboy的文件
find / -type f -user oldboy ! -group oldboy

