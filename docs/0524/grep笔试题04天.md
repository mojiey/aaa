```bash
1 使用grep  取 passwd  显示行数
grep -c passwd <file>

2 使用grep 取passwd   显示行号
grep -n passwd <file>

3   统计test信息在文件中出现了几次
grep -o test <file> | wc -l

4  显示/etc/inittab中包含了:一个数字:(即两个冒号中间一个数字)的行
grep ':[0-9]:' /etc/inittab

5 显示/etc/passwd中以nologin结尾的行 
grep 'nologin$' /etc/passwd

6  显示 /etc/httpd/httpd.conf  注释的行取出来
grep '^#' /etc/httpd/httpd.conf

7 .统计/etc/services文件中有井号开头的行
grep '^#' /etc/services | wc -l




```
