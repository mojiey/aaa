如下都是百度了解扩展 面试题：
```sh
DNS轮询的作用？

1.负载均衡：通过将用户请求分配到不同的服务器上，减少单一服务器的负载压力

2.高可用性：当一个服务器出现故障时，DNS轮询可以自动将请求转发到其他可用的服务器上

3.扩展性：可以轻松地增加或移除服务器
```

**递归查询和迭代查询分别是什么意思?**

- 递归查询是一种请求由一个DNS服务器代为完成所有查询工作的方式
客户端向本地DNS服务器发出一个DNS查询请求,如果该DNS服务器无法直接回答请求,
它会以客户端的代理身份，递归地向其他DNS服务器发出查询请求

- 迭代查询是指DNS服务器在无法直接回答请求时，不会代替客户端去查询，而是告诉客户端下一个可以查询的DNS服务器地址,
客户端需要向这个新的DNS服务器发出查询请求


**在当前目录及子目录中，查找大写字母开头的txt文件**
```sh
find . -type f -name '[A-Z]*.txt'

```

**搜索最近七天内被访问过的所有文件**
```sh
find / -type f -atime -7
```

**搜索超过七天被访问的文件**
```sh
find / -type f -atime +7

```


**工作任务需求：实战 整理好笔记 工作直接参考
企业文件目录增量实时同步 删除不同步  工作用inotify+rsync实时同步实现  实现演示下  %100熟练**

```bash
数据服务器: 10.0.0.7
备份服务器: 10.0.0.8

# 1 备份服务器配置 rsync, /etc/rsyncd.conf
[backup]
path = /data/backup/
read only = no #指定可读写,默认只读

# 2 指定目录给nobody权限，默认用户以nobody访问此目录
setfacl -m u:nobody:rwx /data/backup/

# 3 启动 rsync服务器
rsync --daemon

# 4 数据服务器编写同步脚本 /data/inotify_rsync.sh
# !/bin/bash

SRC='/data/www/'
DEST='root@10.0.0.8::backup'

rpm -q inotify-tools &> /dev/null || dnf -y install inotify-tools
rpm -q rsync &> /dev/null || dnf -y install rsync

inotifywait -mrq --timefmt '%Y-%m-%d %H:%M:%S' --format '%T %w %f' \
    -e create,delete,moved_to,close_write,attrib ${SRC} | while read DATE TIME DIR FILE
do
    FILEPATH=${DIR}${FILE}
    rsync -az --delete $SRC $DES
    echo "At ${TIME} on ${DATE}, file $FILEPATH was backuped up via rsync" >> /var/log/rsync.log
done

# 5 开机启动 /etc/systemd/system/sync.service
[Unit]
Description=Real-time sync of /data/www to remote server
After=network.target

[Service]
ExecStart=/data/inotfiy_rsync.sh
Restart=always

[Install]
WantedBy=multi-user.target

# 6启动sync服务
systemctl daemon-reload
systemctl enable --now sync

```
