01.nginx代码状态代表啥意思工作可以快速定位故障 
```sh

200 ：请求成功
301 ：永久重定向
302：临时重定向
404：资源不存在
403 ：禁止访问
500：服务器端程序错误，无法完成请求
502：服务器网关错误
```

02.工作任务：
编译nginx1.18 安装 发布静态页面测试 ，并设置开机自动启动nginx
```sh
#!/bin/bash

VERSION=1.24.0
# 默认安装路径/usr/local
INSTALL_DIR=/usr/local/nginx

# 源码包存放路径
SRC_DIR=/usr/local/src
# tar包url和存放路径
# TAR_URL=http://10.0.0.10/linux/nginx-$VERSION.tar.gz
TAR_URL=http://nginx.org/download/nginx-$VERSION.tar.gz
TAR_DIR=/opt
# cpu核数
CPUS=`nproc`


color() {
    echo -e "\033[32m$*\033[0m"
}

color 安装nginx依赖包...
dnf -y -q install make zlib zlib-devel gcc-c++ libtool  pcre openssl openssl-devel wget


if [ ! -e $TAR_DIR/nginx-${VERSION}.tar.gz ]; then
    color 下载源码包
    wget $TAR_URL -P $TAR_DIR
fi
if [ ! -d $SRC_DIR/nginx-$VERSION ]; then
    tar xf "$TAR_DIR/nginx-${VERSION}.tar.gz" -C $SRC_DIR

fi

cd $SRC_DIR/nginx-${VERSION}

# 创建用户和组
# groupadd nginx
id nginx &> /dev/null || useradd -r -s /sbin/nologin nginx

color 正在编译
./configure --user=nginx --group=nginx --with-http_ssl_module --with-http_v2_module \
--with-http_stub_status_module --with-http_gzip_static_module \
--with-pcre --with-stream --with-stream_ssl_module > /dev/null

make -j $CPUS > /dev/null && make install
ln -s $INSTALL_DIR/sbin/nginx /usr/sbin/nginx

# 修改默认配置文件
sed -ri '/^#pid/a pid    /run/nginx.pid;' $INSTALL_DIR/conf/nginx.conf
sed -ri 's/^(worker_process.*)1;/\1auto;/' $INSTALL_DIR/conf/nginx.conf
[[ ! -e /usr/local/nginx/conf/conf.d ]] && mkdir $INSTALL_DIR/conf/conf.d
sed -ri '$i \    include conf.d/*.conf;' $INSTALL_DIR/conf/nginx.conf

# 创建nginx启动脚本,参考yum安装时的配置文件
cat > /lib/systemd/system/nginx.service << EOF
[Unit]
Description=The NGINX HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=$INSTALL_DIR/sbin/nginx -t
ExecStart=$INSTALL_DIR/sbin/nginx
ExecStop=$INSTALL_DIR/sbin/nginx -s stop
ExecReload=$INSTALL_DIR/sbin/nginx -s reload
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF


color "\n安装完成\n"

```

**静态页面测试**
```bash
mkdir /data/www/
echo 'hello world' > /data/www/index.html

# 配置nginx.conf
location / {
    root /data/www;
    index index.html;
}

```

**03.设置域名访问： web.it.com 能打开静态页面**
```bash
server {
    listen 80;
    server_name web.it.com;

    root /data/www/web.it.com;
    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

# web.it.com 配置 域名解析
```




**04 利用nginx服务搭建一个多网站(www.it.com bbs.it.com blog.it.com)**

```bash
mkdir -p /data/www/{www,bbs,blog}

for name in {www,bbs,blog};do 
    echo "hello ${name}.it.com" > /data/www/${name}/index.html  
done
```

**配置www.it.com**
```bash
server {
    listen 80;
    server_name www.it.com;

    root /data/www/www;
    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

```
**配置bbs.it.com**
```bash
server {
    listen 80;
    server_name bbs.it.com;

    root /data/www/bbs;
    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

```
**配置blog.it.com**
```bash
server {
    listen 80;
    server_name blog.it.com;
    root /data/www/blog;
    index index.html;


    location / {
        try_files $uri $uri/ =404;
    }
}

```


**05  实战nginx优化方面：用户访问网站如果出现404或者502页面 自动跳转打开    维护中页面   weihu.html**
```bash
# 1 网站根目录创建weihu.html
echo '正在维护' > /data/www/weihu.html

# 2 配置网站 server块
server {
    listen 80;
    server_name blog.it.com;

    root /data/www/blog;
    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }

    error_page 404 502 = /weihu.html;
    location = /weihu.html {
        root /data/www;
        internal;
    }
}

# 3 访问 blog.it.com/404.html

```

