import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Luozaibo",
  outDir: "../public",
  description: "A VitePress Site",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      // { text: 'Home', link: '/' },
      // { text: 'Examples', link: '/markdown-examples' }
    ],

    sidebar: [
      // {
        // text: '0524',
        // collapsed: true,
        // items: [
          // { text: 'awk笔试题01天', link: '/0524/awk笔试题01天' },
          // { text: 'find笔试题第02天', link: '/0524/find笔试题第02天' },
          // { text: 'grep笔试题04天', link: '/0524/grep笔试题04天' },
          // { text: '第14天面试练习题', link: '/0524/第14天面试练习题' },
          // { text: '第15天面试练习题', link: '/0525/第14天面试练习题' },
          // { text: '第16天面试练习题', link: '/0526/第14天面试练习题' },
          // { text: '第17天面试练习题', link: '/0527/第14天面试练习题' },
          // { text: '第18天笔试题', link: '/0524/第18天笔试题' },
          // { text: '第19-20笔试题', link: '/0524/第19-20笔试题' },
        // ]
      // },
      {
        text: '0527',
        collapsed: true,
        items: [
          { text: '第30天任务', link: '/0527/' },
          { text: '第31-33天工作任务', link: '/0527/第31-33天工作任务' },
          { text: '第34-35天工作常用任务', link: '/0527/第34-35天工作常用任务' },
          { text: '第36-39天项目和故障处理', link: '/0527/第36-39天项目和故障处理' }
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  }
})
